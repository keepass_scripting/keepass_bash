#! /bin/bash
#create the salt
getInitpath=$(which list-pass.sh)
initPath=$(echo $getInitpath |sed 's/list-pass.sh/init.ini/')
pfad=$(cat $initPath|head -1)

if [[ $? -ne 0 ]]
then
	echo "please run init.sh"
	exit 1
fi

tuz=`cat ~/.ssh/id_rsa | tr -dc '0-9' | head -c 16`
#Use the salt to unlock the DB pass
Version=$(cat $initPath |tail -2|head -1)
if [ $Version == "111" ]
then
	gatekeeper=`cat  ~/.ssh/salted | openssl enc -aes256 -pbkdf2 -S ${tuz} -d -pass pass:`
else
	gatekeeper=`cat  ~/.ssh/salted | openssl aes-256-cbc -d -a -pass pass:$tuz`
fi
#send it to expect script with the name of the DB entry that is wanted
rm -rf test.txt
#echo $pfad
bash -c "querydb.sh $gatekeeper $1 $pfad" >> test.txt
# Parse it output of expect
Pass=$(cat test.txt|head -3|tail -1)
Username=$(cat test.txt|head -4|tail -1)
#cat test.txt
rm -rf test.txt
echo $Pass #| pbcopy  # send it to clipboard
echo "$Username" # display the user name
