#! /usr/bin/expect 

log_user 0
# set the variables Pimn to unlock Entity to get the required data
set Pin [lindex $argv 0 ]
set Entity [lindex $argv 1] 
set pfad [lindex $argv 2] 
#call the DB
puts "path $pfad"
spawn /usr/bin/keepassxc-cli show -s -a Password -a Username $pfad $Entity 
expect {
    "Enter password to unlock $pfad:" {
        send "$Pin\r"
    }
    timeout {
        puts "Timeout occurred while waiting for the password prompt"
        exit 1
    }
    eof {
        puts "EOF reached"
        exit 1
    }
}
interact
